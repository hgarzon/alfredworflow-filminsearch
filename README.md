# Workflow de Alfred para buscar películas en el catálogo de Filmin
Workflow para Alfred que permite buscar peliculas en el catálogo de [Filmin](http://www.filmin.es) y mostrar los resultados en una lista. Pulsando en un elemento de esa lista se mostrará la ficha de la película en el navegador por defecto.
También se muestra la nota de la película asi como el director de la misma.