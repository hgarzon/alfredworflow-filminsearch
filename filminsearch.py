import sys
from workflow import Workflow
from lxml import html
import requests

query = "{query}"


def main(wf):
	page = requests.get('https://www.filmin.es/busqueda/text/'+query)
	tree = html.fromstring(page.content)
	divs = tree.xpath("//div[contains(@class,'card-film')]")
	
	for div in divs:
		tag_figcaption=div.getchildren()[0].getchildren()[0].getchildren()[1]
		note_value=tag_figcaption.getchildren()[0].text
		director=tag_figcaption.getchildren()[2].text
		tag_link=div.getchildren()[0].getchildren()[0].getchildren()[0]
		tag_img=tag_link.getchildren()[0]
		film_title=tag_img.attrib.get('alt')
		url=tag_link.attrib.get('href')

		icon_filename=''
		if "item-out-of-subscription" in div.attrib.get('class'):
			icon_filename='money.png'
		subtitle_text="De: " + director + " ["+ note_value+"]"
		wf.add_item(film_title, subtitle_text,valid=True, arg=url, icon=icon_filename)
	wf.send_feedback()


if __name__ == u"__main__":
    wf = Workflow()
    sys.exit(wf.run(main))